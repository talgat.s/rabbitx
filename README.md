# Orderbook

Sample project for [rabbitx](https://rabbitx.io).

I have finished project. But I am not good with designs. So this is how it looks
like:

![screenshot](./docs/Screenshot.png)

In order to run first create `.env`. We only use:

- `VITE_RABBITX_WS`
- `VITE_RABBITX_REST`
- `VITE_PUBLIC_JWT`

But others might be needed for utils functions.

And to start in dev env:

```shell
$ npm run dev
```
