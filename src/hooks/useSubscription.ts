import { useEffect, useReducer } from "react";
import {
  Centrifuge,
  ErrorContext,
  PublicationContext,
  SubscribedContext,
  Subscription,
} from "centrifuge";
import Data from "../types/Data.ts";
import mergeData from "../utils/mergeData";

type SubscriptionState = {
  subscription: null | Subscription;
  subscribing: boolean;
  error: null | Error;
  data: null | Data;
};

const initialState = {
  subscription: null,
  subscribing: false,
  error: null,
  data: null,
};

type SubscriptionActions =
  | { type: "add subscription"; payload: Subscription }
  | { type: "subscribing" }
  | { type: "subscribed"; payload: Data }
  | { type: "publication"; payload: Data }
  | { type: "unsubscribed" }
  | { type: "error"; payload: Error }
  | { type: "reset" };

function reducer(
  state: SubscriptionState,
  action: SubscriptionActions,
): SubscriptionState {
  switch (action.type) {
    case "add subscription":
      return {
        subscription: action.payload,
        subscribing: false,
        error: null,
        data: null,
      };
    case "subscribing":
      return {
        subscription: state.subscription,
        subscribing: true,
        error: null,
        data: null,
      };
    case "subscribed":
      return {
        subscription: state.subscription,
        subscribing: false,
        error: null,
        data: action.payload,
      };
    case "publication":
      return {
        subscription: state.subscription,
        subscribing: false,
        error: null,
        data: state.data
          ? mergeData(state.data, action.payload)
          : action.payload,
      };
    case "unsubscribed":
      return {
        subscription: state.subscription,
        subscribing: false,
        error: null,
        data: null,
      };
    case "error":
      return {
        subscription: state.subscription,
        subscribing: false,
        error: action.payload,
        data: null,
      };
    case "reset":
      return initialState;
    default:
      throw Error("unknown action");
  }
}

export function useSubscription(client: Centrifuge, channel: string) {
  const [state, dispatch] = useReducer(reducer, initialState);

  useEffect(() => {
    const subscription =
      client.getSubscription(channel) || client.newSubscription(channel);
    dispatch({ type: "add subscription", payload: subscription });

    return () => client.removeSubscription(subscription);
  }, [client, channel]);

  useEffect(() => {
    if (state.data?.sequence === -1) {
      state.subscription?.unsubscribe();
      state.subscription?.subscribe();
    }
  }, [state.subscription, state.data?.sequence]);

  useEffect(() => {
    if (state.subscription) {
      const subscribingListener = () => {
        dispatch({ type: "subscribing" });
      };
      const subscribedListener = (ctx: SubscribedContext) => {
        dispatch({ type: "subscribed", payload: ctx.data });
      };
      const publicationListener = (ctx: PublicationContext) => {
        dispatch({ type: "publication", payload: ctx.data });
      };
      const unsubscribedListener = () => {
        dispatch({ type: "unsubscribed" });
      };
      const errorListener = (ctx: ErrorContext) => {
        dispatch({ type: "error", payload: new Error(ctx.error.message) });
      };

      state.subscription.on("subscribing", subscribingListener);
      state.subscription.on("subscribed", subscribedListener);
      state.subscription.on("publication", publicationListener);
      state.subscription.on("unsubscribed", unsubscribedListener);
      state.subscription.on("error", errorListener);

      return () => {
        state.subscription?.removeListener("subscribing", subscribingListener);
        state.subscription?.removeListener("subscribed", subscribedListener);
        state.subscription?.removeListener("publication", publicationListener);
        state.subscription?.removeListener(
          "unsubscribed",
          unsubscribedListener,
        );
        state.subscription?.removeListener("error", errorListener);
      };
    }
  }, [state.subscription]);

  return state;
}
