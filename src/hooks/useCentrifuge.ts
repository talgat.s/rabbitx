import { useEffect, useReducer, useRef } from "react";
import { Centrifuge, ErrorContext } from "centrifuge";

type CentrifugeState = {
  centrifuge: Centrifuge;
  connecting: boolean;
  connected: boolean;
  error: null | Error;
};

type CentrifugeActions =
  | { type: "connecting" }
  | { type: "connected" }
  | { type: "disconnected" }
  | { type: "error"; payload: Error };

function reducer(
  state: CentrifugeState,
  action: CentrifugeActions,
): CentrifugeState {
  switch (action.type) {
    case "connecting":
      return {
        centrifuge: state.centrifuge,
        connecting: true,
        connected: false,
        error: null,
      };
    case "connected":
      return {
        centrifuge: state.centrifuge,
        connecting: false,
        connected: true,
        error: null,
      };
    case "disconnected":
      return {
        centrifuge: state.centrifuge,
        connecting: false,
        connected: false,
        error: null,
      };
    case "error":
      return {
        centrifuge: state.centrifuge,
        connecting: false,
        connected: false,
        error: action.payload,
      };
    default:
      throw Error("unknown action");
  }
}

export function useCentrifuge() {
  const centrifugeRef = useRef(
    new Centrifuge(import.meta.env.VITE_RABBITX_WS, {
      token: import.meta.env.VITE_PUBLIC_JWT,
    }),
  );

  const [state, dispatch] = useReducer(reducer, {
    centrifuge: centrifugeRef.current,
    connecting: false,
    connected: false,
    error: null,
  });

  useEffect(() => {
    const connectingListener = () => {
      dispatch({ type: "connecting" });
    };
    const connectedListener = () => {
      dispatch({ type: "connected" });
    };
    const disconnectedListener = () => {
      dispatch({ type: "disconnected" });
    };
    const errorListener = (ctx: ErrorContext) => {
      dispatch({ type: "error", payload: new Error(ctx.error.message) });
    };

    centrifugeRef.current.on("connecting", connectingListener);
    centrifugeRef.current.on("connected", connectedListener);
    centrifugeRef.current.on("disconnected", disconnectedListener);
    centrifugeRef.current.on("error", errorListener);

    return () => {
      centrifugeRef.current.removeListener("connecting", connectingListener);
      centrifugeRef.current.removeListener("connected", connectedListener);
      centrifugeRef.current.removeListener(
        "disconnected",
        disconnectedListener,
      );
      centrifugeRef.current.removeListener("error", errorListener);
    };
  }, []);

  return state;
}
