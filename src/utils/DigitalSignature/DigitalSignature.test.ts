import { describe, expect, test } from "vitest";
import DigitalSignature from "./DigitalSignature.ts";

describe("test - utils/DigitalSignature", () => {
  const secret = "0xdeadbeef";

  test("should sign correctly", async () => {
    const expectedSignature =
      "0xce21fc10e3d5d4f3db050ccadf2a8bad05835667b2782f99eccc4bc659b43903";

    const data = { method: "POST", path: "/" };
    const timestamp = 1698350443 + 1; // add a second;

    const digitalSignature = new DigitalSignature(data, timestamp);
    await expect(digitalSignature.sign(secret)).resolves.toBe(
      expectedSignature,
    );
  });
});
