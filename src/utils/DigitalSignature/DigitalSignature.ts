import hex from "../hex";
import sha256 from "../sha256";

class DigitalSignature {
  #data: Record<string, unknown>;
  #timestamp: number;

  constructor(data: Record<string, unknown>, timestamp: number) {
    this.#data = data;
    this.#timestamp = timestamp;
  }

  async sign(secret: string) {
    const hash = await this.#getHash(this.#data, this.#timestamp);
    const rawKey = await this.#getRawKey(secret);

    const signature = await crypto.subtle.sign(
      {
        name: "HMAC",
        hash: { name: "SHA-256" },
      },
      rawKey,
      hash,
    );

    return hex.toHexString(signature);
  }

  #getRawKey(secret: string) {
    const msgUint8 = hex.toArrayBuffer(secret);

    return crypto.subtle.importKey(
      "raw",
      msgUint8,
      {
        name: "HMAC",
        hash: { name: "SHA-256" },
      },
      true,
      ["sign", "verify"],
    );
  }

  #getHash(data: Record<string, unknown>, timestamp: number) {
    const message = this.#getPayloadMessage(data, timestamp);
    return sha256(message);
  }

  #getPayloadMessage(data: Record<string, unknown>, timestamp: number) {
    const message = this.#getSortedPayloadStr(data);
    return `${message}${timestamp}`;
  }

  #getSortedPayloadStr(payload: Record<string, unknown>) {
    return Object.keys(payload)
      .sort()
      .reduce((acc, key) => `${acc}${key}=${payload[key]}`, "");
  }
}

export default DigitalSignature;
