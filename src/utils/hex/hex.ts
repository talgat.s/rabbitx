class Hex {
  toArrayBuffer(hexStr: string) {
    const typedArray = new Uint8Array(
      (hexStr.replace(/^0x/, "").match(/[\da-f]{2}/gi) ?? []).map(function (h) {
        return parseInt(h, 16);
      }),
    );

    return typedArray.buffer;
  }

  toHexString(buf: ArrayBuffer) {
    const uintArr = new Uint8Array(buf);
    const str = Array.from(uintArr, (byte) =>
      ("0" + (byte & 0xff).toString(16)).slice(-2),
    ).join("");
    return `0x${str}`;
  }
}

const hex = new Hex();

export default hex;
