import getLinuxTimeInSec from "../getLinuxTimeInSec";
import DigitalSignature from "../DigitalSignature";

class RequestData {
  #path: string;
  #method: string;
  #body: Record<string, unknown> | undefined;
  #apiKey: string;
  #secret: string;

  constructor(
    path: string = "/",
    method: string = "GET",
    body: Record<string, unknown> | undefined,
    apiKey: string,
    secret: string,
  ) {
    this.#path = path;
    this.#method = method;
    this.#body = body;
    this.#apiKey = apiKey;
    this.#secret = secret;
  }

  get jsonBody() {
    return this.#body && JSON.stringify(this.#body);
  }

  get path() {
    return this.#path;
  }

  get method() {
    return this.#method;
  }

  async getHeaders() {
    const timestamp = getLinuxTimeInSec();
    const data = {
      ...this.#body,
      path: this.#path,
      method: this.#method,
    };
    const digitalSignature = new DigitalSignature(data, timestamp);
    const signature = await digitalSignature.sign(this.#secret);

    return new Headers({
      "Content-Type": "application/json",
      "RBT-SIGNATURE": signature,
      "RBT-API-KEY": this.#apiKey,
      "RBT-TS": `${timestamp + 2 * 60}`, // add 2 minutes
    });
  }
}

export default RequestData;
