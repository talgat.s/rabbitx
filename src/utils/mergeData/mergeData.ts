import Data from "../../types/Data.ts";

function mergeData(oldData: Data, newData: Data): Data {
  return {
    bids: [...oldData.bids, ...newData.bids].sort((a, b) => {
      if (a[0] > b[0]) {
        return -1;
      }

      if (a[0] < b[0]) {
        return 1;
      }

      return 0;
    }),
    asks: [...oldData.asks, ...newData.asks].sort((a, b) => {
      if (a[0] > b[0]) {
        return 1;
      }

      if (a[0] < b[0]) {
        return -1;
      }

      return 0;
    }),
    market_id: newData.market_id,
    timestamp: newData.timestamp,
    sequence: oldData.sequence + 1 === newData.sequence ? newData.sequence : -1,
  };
}

export default mergeData;
