/// <reference types="vite/client" />

interface ImportMetaEnv {
  readonly VITE_RABBITX_WS: string;
  readonly VITE_RABBITX_REST: string;
  readonly VITE_API_KEY: string;
  readonly VITE_SECRET: string;
  readonly VITE_PUBLIC_JWT: string;
  readonly VITE_PRIVATE_JWT: string;
  readonly VITE_REFRESH_TOKEN: string;
  // more env variables...
}

interface ImportMeta {
  readonly env: ImportMetaEnv;
}
