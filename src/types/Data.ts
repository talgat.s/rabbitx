type Data = {
  asks: [string, string][];
  bids: [string, string][];
  market_id: string;
  sequence: number;
  timestamp: number;
};

export default Data;
