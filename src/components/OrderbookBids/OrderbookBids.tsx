import cl from "clsx";

type Props = {
  data: [string, string][];
};

const OrderbookBids = ({ data }: Props) => {
  const renderData = [...data]
    .reverse()
    .reduce<[number, number, number][]>((acc, tuple) => {
      const prev = acc.at(-1)?.at(2) ?? 0;
      return [
        ...acc,
        [
          Number(tuple[0]),
          Number(tuple[1]),
          Math.round((Number(tuple[1]) + prev + Number.EPSILON) * 10000) /
            10000,
        ],
      ];
    }, []);

  return (
    <div className="relative overflow-x-auto shadow-md sm:rounded-lg">
      <table className="w-full text-left text-sm text-gray-500 dark:text-gray-400">
        <thead className="bg-gray-50 text-xs uppercase text-gray-700 dark:bg-gray-700 dark:text-gray-400">
          <tr>
            <th scope="col" className="px-6 py-3">
              <span>Price</span>
              <span className="mr-2 rounded bg-gray-100 px-2.5 py-0.5 text-xs font-medium text-gray-800 dark:bg-gray-700 dark:text-gray-300">
                USD
              </span>
            </th>
            <th scope="col" className="px-6 py-3">
              <span>Amount</span>
              <span className="mr-2 rounded bg-gray-100 px-2.5 py-0.5 text-xs font-medium text-gray-800 dark:bg-gray-700 dark:text-gray-300">
                BTC
              </span>
            </th>
            <th scope="col" className="px-6 py-3">
              <span>Total</span>
              <span className="mr-2 rounded bg-gray-100 px-2.5 py-0.5 text-xs font-medium text-gray-800 dark:bg-gray-700 dark:text-gray-300">
                BTC
              </span>
            </th>
          </tr>
        </thead>
        <tbody>
          {renderData.map((tuple, i) => (
            <tr
              className={cl(
                "border-b dark:border-gray-700",
                i % 2 === 0
                  ? "bg-white dark:bg-gray-900"
                  : "bg-gray-50 dark:bg-gray-800",
              )}
            >
              <th
                scope="row"
                className="whitespace-nowrap px-6 py-4 font-medium text-gray-900 dark:text-white"
              >
                {tuple[0]}
              </th>
              <td className="px-6 py-4">{tuple[1]}</td>
              <td className="px-6 py-4">{tuple[2]}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default OrderbookBids;
